function find(elements,cb){
    if(Array.isArray(elements)){
        for(i=0;i<elements.length;i++){
            let result = cb(elements[i],i,elements);
            if(result){
                return elements[i];
            }
        }
        return undefined;
    }
}

module.exports = find;