function reduce(elements,cb,initial){
    let prev = initial;
    let currIndex = 0;
    if(elements.length === 0 && initial === undefined){
        throw new TypeError("Reduce of empty array with no initial value");
    }
    
    if(initial === undefined){
        prev = elements[0];
        currIndex = 1;
    }
    for(let index = currIndex;index<elements.length;index++){
        if(elements[index] === undefined){
            continue;
        }
        prev = cb(prev,elements[index],index,elements);
        
    }
    return prev;
}


module.exports = reduce;