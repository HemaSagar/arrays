function map(elements,cb){
    if(Array.isArray(elements)){
        let result = [];
        for(let index=0;index<elements.length;index++){
            result.push(cb(elements[index],index,elements));
        }

        return result;
    }
}

module.exports = map;