const reduce = require('../reduce.js')

const items = [];


try {
    const result = reduce(items, function cb(previousValue, currentValue) {
        return previousValue + currentValue;
    });
    console.log(result);

}

catch (e) {
    console.error(e);
}
