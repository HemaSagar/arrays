const each = require('../each.js')

//Given array for testing
const items = [1, 2, 3, 4, 5, 5];
const itemsSquared = [];

//calling the each, which pushes the square of a element in the given array to a new array itemsSquared.
each("hi", function square(element, index, arr){
    itemsSquared.push(element * element);
})

// console.log(itemsSquared);