function flatten(array, depth = 1) {
  let flattened = [];

  if (depth < 1) {
    return array;
  }
  for (let index = 0; index < array.length; index++) {
    if (array[index] == undefined) {
      delete array[index];
      continue;
    }

    if (array[index] instanceof Array) {
      let newArray = flatten(array[index], depth - 1);
      flattened = flattened.concat(newArray);
    }

    else {
      flattened.push(array[index])
    }
  }
return flattened;
}








module.exports = flatten;